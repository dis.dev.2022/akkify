"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import maskInput from './forms/input-mask.js'; // Маска ввода для форм
import mobileNav from './components/mobile-nav.js';  // Мобильное меню
import Quantity from './forms/quantity.js';  // Мобильное меню
import HystModal from 'hystmodal';

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();


// Маска для ввода номера телефона
maskInput('input[name="phone"]');

// Меню для мобильной версии
mobileNav();

// Input number
Quantity()

// Modal
const myModal = new HystModal({
    linkAttributeName: 'data-hystmodal',
    catchFocus: false
});

(() => {

    document.addEventListener('change', (event) => {
        if (event.target.closest('[data-form-accept]')) {
            let btnSubmit = event.target.closest('[data-form]').querySelector('[data-form-submit]')
            if (event.target.closest('[data-form-accept]').checked) {
                btnSubmit.removeAttribute("disabled");
            } else {
                btnSubmit.setAttribute("disabled", true);
            }
        }
    });
})();

